<?php include_once "./code.php" ?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>S03: Classes, Objects, Inheritance and Polymorphism</title>
  <link rel="stylesheet" href="./style.php" media="screen">
</head>

<body>
  <div class="container">
    <h1>Person</h1>
    <p>
      <?php 
      $person = new Person("Peter", null, "Mendres");
      echo $person->printName();
      ?>
    </p>
  </div>

  <div class="container">
    <h1>Developer</h1>
    <p>
      <?php 
      $developer = new Developer("Vilmar", "Agustin", "Cabañero");
      echo $developer->printName();
      ?>
    </p>
  </div>

  <div class="container">
    <h1>Engineer</h1>
    <p>
      <?php 
      $engineer = new Engineer("Sherwin", "Cabañero", "Empino");
      echo $engineer->printName();
      ?>
    </p>
  </div>
</body>

</html>